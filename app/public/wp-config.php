<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Nffs+urWYvZpPRh8YQkiAhmHR4uNpiykU8xDiUxoMs/48jwU6EKTg0cvwRfTsPD/BbWYBNrceMYOAzmOrzWABA==');
define('SECURE_AUTH_KEY',  '2tIpYSEufqk+vGEekAW/yLEyJr6D+9cmkR8eJAsOwo+KMNcpJ/883C7iGNvB/Mh9z4bQT1GuuzWGrj4G9DI2pA==');
define('LOGGED_IN_KEY',    'UWh2zhmYLqCCy7wVTUhqYoXDEpe4ch7W4wjPHVfc5dpf+NIdt0PmIydpl8YRJ8BXYsbbgdw/OXZC8Iy4ErfRBg==');
define('NONCE_KEY',        'TsAJDF5PTJWZfo4RbgOWw4N8q4Yx+Q7B/nZxq9+7YbwtQzC5KcAlcCsf+DDQvDSYcSlzyoOUHU/w+eX8oIn15w==');
define('AUTH_SALT',        'D0x6Hos3K+OoaU/r6tOp2o3V6HNVo2GQ5Ra375ddnNPAu/CrxNU3FN2gR7WHJ74uGv8fIV8FuNb+dvCugho4JQ==');
define('SECURE_AUTH_SALT', 'iupYFysm1wperq9eYIa/uB2h5D15zkaAIcuCgryW+DiUTqnew0VrmqcSjfAIvy1EOe3Et6bmpLu/DXrEaaTBQg==');
define('LOGGED_IN_SALT',   'WuzfjRTzEIRgZkMjjf6uvL+ZM1E4DlMXK0Wt0CYe9goC/TEeyE7FmslhZGh7qPYyk6MxDl3UsmX6oCmJMrkNdA==');
define('NONCE_SALT',       'cadqxd9YqbFqB2NCD/Jiid7kp0MgXUqPLXXFMHP7Wbf5D9Hxym92CUl5v7zCzhhrhMspsDtlZQZk8YQ6PnHFDQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
