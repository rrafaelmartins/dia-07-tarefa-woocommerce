<?php
/**
 * Home starter content.
 *
 * @package Cartsy_Lite\Inc\Starter_Content
 */
return [
	'post_type'  => 'page',
	'post_title' => _x( 'Home', 'Theme starter content', 'cartsy-lite' ),
	'template'   => 'template-homepage.php',
	'post_content' => '
	<!-- wp:spacer {"height":1} -->
	<div style="height:1px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

	<!-- wp:group {"align":"full","style":{"color":{"text":"#000000","background":"#ffffff"}}} -->
	<div class="wp-block-group alignfull has-text-color has-background" style="background-color:#ffffff;color:#000000"><!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

	<!-- wp:columns {"align":"wide"} -->
	<div class="wp-block-columns alignwide"><!-- wp:column -->
	<div class="wp-block-column"><!-- wp:image {"id":728,"sizeSlug":"large","linkDestination":"none"} -->
	<figure class="wp-block-image size-large"><img src="https://s.w.org/patterns/files/2021/06/insects-and-flowers-white-1024x925.jpg" alt="An illustration of white flowers and insects." class="wp-image-728"/></figure>
	<!-- /wp:image -->

	<!-- wp:heading {"level":3,"style":{"typography":{"fontSize":35,"lineHeight":"1.26"}},"className":"margin-bottom-half"} -->
	<h3 class="margin-bottom-half" id="flowers" style="font-size:35px;line-height:1.26">Flowers</h3>
	<!-- /wp:heading -->

	<!-- wp:paragraph {"style":{"typography":{"fontSize":17,"lineHeight":"1.65"}},"className":"margin-top-half"} -->
	<p class="margin-top-half" style="font-size:17px;line-height:1.65">Bees are some of natures most adept pollinators. They collect pollen in small sacs near their legs, dropping some along the way to help fertilize flowers. </p>
	<!-- /wp:paragraph --></div>
	<!-- /wp:column -->

	<!-- wp:column -->
	<div class="wp-block-column"><!-- wp:image {"id":729,"sizeSlug":"large","linkDestination":"none"} -->
	<figure class="wp-block-image size-large"><img src="https://s.w.org/patterns/files/2021/06/insects-and-flowers-yellow-1024x930.jpg" alt="An illustration of yellow flowers and insects." class="wp-image-729"/></figure>
	<!-- /wp:image -->

	<!-- wp:heading {"level":3,"style":{"typography":{"fontSize":35,"lineHeight":"1.26"}},"className":"margin-bottom-half"} -->
	<h3 class="margin-bottom-half" id="insects" style="font-size:35px;line-height:1.26">Insects</h3>
	<!-- /wp:heading -->

	<!-- wp:paragraph {"style":{"typography":{"fontSize":17,"lineHeight":"1.65"}},"className":"margin-top-half"} -->
	<p class="margin-top-half" style="font-size:17px;line-height:1.65">Bees arent the only pollinators. Insects such as butterflies, moths, bee flies, mosquitoes, and even ants fertilize flowers as they travel around your yard. </p>
	<!-- /wp:paragraph --></div>
	<!-- /wp:column --></div>
	<!-- /wp:columns -->

	<!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer --></div>
	<!-- /wp:group -->

	<!-- wp:group {"align":"full","style":{"color":{"text":"#000000","background":"#ffffff"}}} -->
	<div class="wp-block-group alignfull has-text-color has-background" style="background-color:#ffffff;color:#000000"><!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

	<!-- wp:columns {"align":"wide"} -->
	<div class="wp-block-columns alignwide"><!-- wp:column -->
	<div class="wp-block-column"><!-- wp:cover {"customOverlayColor":"#f6f6f6","minHeight":600,"isDark":false,"className":"is-style-cartsy-lite-border"} -->
	<div class="wp-block-cover is-light is-style-cartsy-lite-border" style="min-height:600px"><span aria-hidden="true" class="has-background-dim-100 wp-block-cover__gradient-background has-background-dim" style="background-color:#f6f6f6"></span><div class="wp-block-cover__inner-container"><!-- wp:image {"align":"center","id":571,"sizeSlug":"medium","linkDestination":"none"} -->
	<div class="wp-block-image"><figure class="aligncenter size-medium"><img src="https://s.w.org/patterns/files/2021/06/wire-sculpture-263x300.jpg" alt="" class="wp-image-571"/></figure></div>
	<!-- /wp:image --></div></div>
	<!-- /wp:cover --></div>
	<!-- /wp:column -->

	<!-- wp:column {"verticalAlignment":"center"} -->
	<div class="wp-block-column is-vertically-aligned-center"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":35,"lineHeight":"1.26"}},"className":"margin-bottom-half"} -->
	<h3 class="margin-bottom-half" id="insects" style="font-size:35px;line-height:1.26">Insects</h3>
	<!-- /wp:heading -->

	<!-- wp:paragraph {"style":{"typography":{"fontSize":17,"lineHeight":"1.65"}},"className":"margin-top-half"} -->
	<p class="margin-top-half" style="font-size:17px;line-height:1.65">Bees arent the only pollinators. Insects such as butterflies, moths, bee flies, mosquitoes, and even ants fertilize flowers as they travel around your yard. </p>
	<!-- /wp:paragraph --></div>
	<!-- /wp:column --></div>
	<!-- /wp:columns -->

	<!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer --></div>
	<!-- /wp:group -->

	<!-- wp:group {"align":"full","style":{"color":{"text":"#000000","background":"#212121"}}} -->
	<div class="wp-block-group alignfull has-text-color has-background" style="background-color:#212121;color:#000000"><!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

	<!-- wp:columns {"align":"wide"} -->
	<div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center"} -->
	<div class="wp-block-column is-vertically-aligned-center"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":35,"lineHeight":"1.26"}},"textColor":"white","className":"margin-bottom-half"} -->
	<h3 class="margin-bottom-half has-white-color has-text-color" id="insects" style="font-size:35px;line-height:1.26">Insects</h3>
	<!-- /wp:heading -->

	<!-- wp:paragraph {"style":{"typography":{"fontSize":17,"lineHeight":"1.65"}},"textColor":"white","className":"margin-top-half"} -->
	<p class="margin-top-half has-white-color has-text-color" style="font-size:17px;line-height:1.65">Bees arent the only pollinators. Insects such as butterflies, moths, bee flies, mosquitoes, and even ants fertilize flowers as they travel around your yard. </p>
	<!-- /wp:paragraph --></div>
	<!-- /wp:column -->

	<!-- wp:column -->
	<div class="wp-block-column"><!-- wp:cover {"customOverlayColor":"#f6f6f6","minHeight":600,"isDark":false,"className":"is-style-cartsy-lite-border"} -->
	<div class="wp-block-cover is-light is-style-cartsy-lite-border" style="min-height:600px"><span aria-hidden="true" class="has-background-dim-100 wp-block-cover__gradient-background has-background-dim" style="background-color:#f6f6f6"></span><div class="wp-block-cover__inner-container"><!-- wp:image {"align":"center","id":571,"sizeSlug":"medium","linkDestination":"none"} -->
	<div class="wp-block-image"><figure class="aligncenter size-medium"><img src="https://s.w.org/patterns/files/2021/06/wire-sculpture-263x300.jpg" alt="" class="wp-image-571"/></figure></div>
	<!-- /wp:image --></div></div>
	<!-- /wp:cover --></div>
	<!-- /wp:column --></div>
	<!-- /wp:columns -->

	<!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer --></div>
	<!-- /wp:group -->

	<!-- wp:group {"align":"full","style":{"color":{"text":"#000000","background":"#ffffff"}}} -->
	<div class="wp-block-group alignfull has-text-color has-background" style="background-color:#ffffff;color:#000000"><!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

	<!-- wp:columns {"align":"wide"} -->
	<div class="wp-block-columns alignwide"><!-- wp:column -->
	<div class="wp-block-column"><!-- wp:cover {"customOverlayColor":"#f6f6f6","minHeight":600,"isDark":false,"className":"is-style-cartsy-lite-border"} -->
	<div class="wp-block-cover is-light is-style-cartsy-lite-border" style="min-height:600px"><span aria-hidden="true" class="has-background-dim-100 wp-block-cover__gradient-background has-background-dim" style="background-color:#f6f6f6"></span><div class="wp-block-cover__inner-container"><!-- wp:image {"align":"center","id":571,"sizeSlug":"medium","linkDestination":"none"} -->
	<div class="wp-block-image"><figure class="aligncenter size-medium"><img src="https://s.w.org/patterns/files/2021/06/wire-sculpture-263x300.jpg" alt="" class="wp-image-571"/></figure></div>
	<!-- /wp:image --></div></div>
	<!-- /wp:cover --></div>
	<!-- /wp:column -->

	<!-- wp:column {"verticalAlignment":"center"} -->
	<div class="wp-block-column is-vertically-aligned-center"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":35,"lineHeight":"1.26"}},"className":"margin-bottom-half"} -->
	<h3 class="margin-bottom-half" id="insects" style="font-size:35px;line-height:1.26">Insects</h3>
	<!-- /wp:heading -->

	<!-- wp:paragraph {"style":{"typography":{"fontSize":17,"lineHeight":"1.65"}},"className":"margin-top-half"} -->
	<p class="margin-top-half" style="font-size:17px;line-height:1.65">Bees arent the only pollinators. Insects such as butterflies, moths, bee flies, mosquitoes, and even ants fertilize flowers as they travel around your yard. </p>
	<!-- /wp:paragraph --></div>
	<!-- /wp:column --></div>
	<!-- /wp:columns -->

	<!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer --></div>
	<!-- /wp:group -->

	<!-- wp:group {"align":"full","style":{"color":{"text":"#000000","background":"#ffffff"}}} -->
	<div class="wp-block-group alignfull has-text-color has-background" style="background-color:#ffffff;color:#000000"><!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

	<!-- wp:columns {"align":"wide"} -->
	<div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center"} -->
	<div class="wp-block-column is-vertically-aligned-center"><!-- wp:separator {"className":"is-style-default"} -->
	<hr class="wp-block-separator is-style-default"/>
	<!-- /wp:separator -->

	<!-- wp:image {"align":"center","width":150,"height":150,"sizeSlug":"large","linkDestination":"none","className":"is-style-rounded"} -->
	<div class="wp-block-image is-style-rounded"><figure class="aligncenter size-large is-resized"><img src="https://s.w.org/images/core/5.8/portrait.jpg" alt="A side profile of a woman in a russet-colored turtleneck and white bag. She looks up with her eyes closed." width="150" height="150"/></figure></div>
	<!-- /wp:image -->

	<!-- wp:quote {"align":"center","className":"is-style-large"} -->
	<blockquote class="wp-block-quote has-text-align-center is-style-large"><p>"Contributing makes me feel like Im being useful to the planet."</p><cite>— Anna Wong, <em>Volunteer</em></cite></blockquote>
	<!-- /wp:quote -->

	<!-- wp:separator {"className":"is-style-default"} -->
	<hr class="wp-block-separator is-style-default"/>
	<!-- /wp:separator --></div>
	<!-- /wp:column --></div>
	<!-- /wp:columns -->

	<!-- wp:spacer {"height":64} -->
	<div style="height:64px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer --></div>
	<!-- /wp:group -->
	',
];
